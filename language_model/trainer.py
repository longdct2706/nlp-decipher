from tqdm import tqdm

import torch
import torch.nn.functional as F

from language_model.prepare_data import prepare_data
from language_model.utils import random_training_set, eval_set, tensor_to_string


class Trainer:
    def __init__(self,
                 model,
                 criterion,
                 optimizer,
                 config):
        self.model = model
        self.criterion = criterion
        self.optimizer = optimizer

        self.config = config

        self.train_data, self.valid_data, self.test_data = None, None, None
        self.prepare_data()

    def prepare_data(self):
        sequence = prepare_data()
        train_len = int(len(sequence) * (1 - self.config.test_size))
        valid_len = int(train_len * self.config.valid_size)
        train_len -= valid_len
        self.train_data = sequence[:train_len]
        self.valid_data = sequence[train_len:train_len + valid_len]
        self.test_data = sequence[train_len + valid_len:]
        print('Finish preparing data')

    def train(self):
        sequence = self.train_data
        print("Training for %d epochs..." % self.config.epoch)
        loss = []
        best_acc = 0
        best_epoch = None
        counter_patience = 0
        pbar = tqdm(range(1, self.config.epoch + 1))
        for epoch in pbar:
            inp, target = random_training_set(sequence,
                                              self.config.chunk_len,
                                              self.config.batch_size)
            loss.append(self.train_one_epoch(inp, target))

            acc = self.eval('valid')
            if acc <= best_acc:
                counter_patience += 1
                if counter_patience > self.config.train_patience:
                    print(
                        f'\n{counter_patience} epochs without improvement. Train terminated')
                    break
            else:
                best_acc = acc
                best_epoch = epoch
                counter_patience = 0
                self.save()

            pbar.set_description(
                ('Epoch {} Loss {:.4f} Acc {:.4f}'.format(epoch, loss[-1], acc)))

        print('Training achieve best accuracy {:.4f} at epoch {}'.format(best_acc, best_epoch))

    def train_one_epoch(self, inp, target):
        h_0, c_0 = self.model.state0(self.config.batch_size)
        hidden = h_0.cuda(), c_0.cuda()
        self.model.train()
        self.model.zero_grad()
        loss = 0

        for c in range(self.config.chunk_len):
            hidden, output = self.model(inp[:, c], hidden)
            loss += self.criterion(output.view(self.config.batch_size, -1),
                                   target[:, c])

        loss.backward()
        self.optimizer.step()

        return loss.item() / self.config.chunk_len

    def eval(self, mode='test'):
        assert mode in ['test', 'valid'], 'Invalid evaluation mode'
        if mode == 'test':
            sequence = self.test_data
        else:
            sequence = self.valid_data

        batch_size = len(sequence) // self.config.chunk_len
        inp, target = eval_set(sequence, self.config.chunk_len, batch_size)

        h_0, c_0 = self.model.state0(batch_size)
        hidden = h_0.cuda(), c_0.cuda()
        self.model.eval()

        correct = 0
        for c in range(self.config.chunk_len):
            hidden, output = self.model(inp[:, c], hidden)
            y_pred = F.softmax(output, dim=1)
            y_pred = torch.argmax(y_pred, dim=1)

            correct += torch.sum(y_pred == target[:, c])
            if mode == 'test':
                print(f'y_true: {tensor_to_string(target[0, c])}')
                print(f'y_pred: {tensor_to_string(y_pred[0])}')

        return correct.item() / target.nelement()

    def save(self, best=True):
        if best:
            save_filename = 'mlstm_best.pt'
        else:
            save_filename = 'mlstm_last.pt'
        torch.save({'state_dict': self.model.state_dict()}, save_filename)
        # print('Saved as %s' % save_filename)
