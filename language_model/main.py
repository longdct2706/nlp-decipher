import torch
import torch.nn as nn

from language_model.models.mlstm import mLSTM, StackedLSTM
from language_model.trainer import Trainer
from language_model.NLM_config import LMConfig, letter


def main(config):
    model = StackedLSTM(mLSTM,
                        config.n_layers,
                        config.embed_size,
                        config.hidden_size,
                        len(letter),
                        config.dropout)
    model = model.cuda()

    criterion = nn.CrossEntropyLoss()
    criterion = criterion.cuda()
    optimizer = torch.optim.Adam(model.parameters(), lr=config.lr)

    trainer = Trainer(model, criterion, optimizer, config)
    trainer.train()
    trainer.save(best=False)

    accuracy = trainer.eval()
    print(accuracy)


if __name__ == '__main__':
    import nltk

    nltk.download('punkt')
    nltk.download('brown')

    config = LMConfig()
    main(config)
