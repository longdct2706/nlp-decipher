import random
import torch

from language_model.NLM_config import letter


def char_tensor(string):
    tensor = torch.zeros(len(string)).long()
    for c in range(len(string)):
        try:
            tensor[c] = letter.index(string[c])
        except:
            continue
    return tensor


def random_training_set(sequence, chunk_len, batch_size):
    inp = torch.LongTensor(batch_size, chunk_len)
    target = torch.LongTensor(batch_size, chunk_len)
    for bi in range(batch_size):
        start_index = random.randint(0, len(sequence) - chunk_len)
        end_index = start_index + chunk_len + 1
        chunk = sequence[start_index:end_index]
        inp[bi] = char_tensor(chunk[:-1])
        target[bi] = char_tensor(chunk[1:])
    inp, target = inp.cuda(), target.cuda()
    return inp, target


def eval_set(sequence, chunk_len, batch_size):
    inp = torch.LongTensor(batch_size, chunk_len)
    target = torch.LongTensor(batch_size, chunk_len)
    for bi in range(batch_size):
        start_index = bi * chunk_len
        end_index = start_index + chunk_len + 1
        chunk = sequence[start_index:end_index]
        inp[bi] = char_tensor(chunk[:-1])
        target[bi] = char_tensor(chunk[1:])
    inp, target = inp.cuda(), target.cuda()
    return inp, target


def tensor_to_string(tensor):
    tensor = tensor.view(-1)
    s = ''
    for idx in tensor:
        s += letter[idx.item()]

    return s


def load_model(model, filepath, gpu=False):
    if gpu:
        state_dict = torch.load(filepath)['state_dict']
    else:
        state_dict = torch.load(filepath, map_location=torch.device('cpu'))['state_dict']
    model.load_state_dict(state_dict)
    return model
