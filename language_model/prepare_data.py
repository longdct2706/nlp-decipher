from nltk.corpus import brown
from language_model.NLM_config import letter


def prepare_data():
    char_set = {'a'}
    for word in brown.words():
        char_set = char_set.union(set(list(word)))
    s = ''.join(brown.words())
    for char in char_set:
        if char not in letter:
            s = s.replace(char, '')
    s = s.lower()
    return s
