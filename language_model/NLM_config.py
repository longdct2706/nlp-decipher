letter = 'abcdefghijklmnopqrstuvwxyz'


class LMConfig:
    def __init__(self):
        self.epoch = 3000
        self.train_patience = 100
        self.valid_size = 0.01
        self.test_size = 0.01
        self.embed_size = 64
        self.hidden_size = 256
        self.n_layers = 2
        self.lr = 0.001
        self.chunk_len = 50
        self.batch_size = 16
        self.dropout = 0.0
        self.shuffle = True
        self.seed = 420
