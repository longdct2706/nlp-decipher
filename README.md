# Decipherment of substitution cipher using NLP
Midterm project for Information Security course.

The project consists of two modules: 
* Neural language model (NLM) using mLSTM
* Decipherment using NLM with beam search

Install the dependencies for this project
```
pip install -r requirements.txt
```

## Neural language model
The configuration for the training process of NLM can be found in language_model/NLM_config.py.

To train the NLM, run
```
python -m language_model.main
```

## Decipherment of substitution cipher
The number of beams for beam search process can be found in decipherment/decipherment_config.py.

Run the decipherment process
```
python -m decipherment.main
```

On average, each decipherment took about 10-30 mins to decipher with 1000 beams.

## TODO
* Increase the search process by using parallel programming
* Change the text preprocessing method to increase the learning capability of the NLM
* Experiment with better NLM model, such as attention and transformer.
