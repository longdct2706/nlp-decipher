import os
from decipherment.decipherment_config import data_path


def read_data(filepath):
    data = []
    with open(filepath, 'r') as f:
        s = f.readlines()
    for string in s:
        string = string.replace(' ', '')
        string = string.replace('\n', '')
        data.append(string)

    return data


def get_cipher_data():
    filepath = os.path.join(data_path, 'cipher_text.txt')
    return read_data(filepath)


def get_plain_data():
    filepath = os.path.join(data_path, 'plain_text.txt')
    return read_data(filepath)
