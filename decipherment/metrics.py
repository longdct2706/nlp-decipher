import numpy as np


def calc_SER(sequence, plain_text):
    assert len(sequence) == len(plain_text), 'Invalid sequence length'
    sequence = np.array(list(sequence))
    plain_text = np.array(list(plain_text))

    error_idx = sequence != plain_text
    return np.sum(error_idx) / len(plain_text)
