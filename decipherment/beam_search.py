import numpy as np
import torch

from decipherment.decipherment_config import letter
from decipherment.generate_sequence import generate_sequence
from decipherment.calculate_score import calc_score


def create_set_characters(cipher_text):
    char_set = []
    for char in cipher_text:
        if char not in char_set:
            char_set.append(char)
    return char_set


def beam_search(cipher_text, model, config):
    plain_text = ''
    for i in range(len(cipher_text)):
        plain_text += '.'

    info_prev_stage = [{'sequence': plain_text,
                        'hypothesis': ''}]
    for char in create_set_characters(cipher_text):
        print(f'Beam search for letter {char}')
        info_prev_stage = beam_search_one_stage(char, cipher_text, info_prev_stage, model, config)

    return [info['sequence'] for info in info_prev_stage]


def beam_search_one_stage(cipher_char, cipher_text, info_prev_stage, model, config):
    info_branch = []
    for info in info_prev_stage:
        info_branch += beam_search_one_branch(cipher_char, cipher_text, info['sequence'], info['hypothesis'], model)

    scores = [info['score'] for info in info_branch]
    sorted_index = np.argsort(scores)
    info_this_stage = []
    for idx in sorted_index[:config.nbeam]:
        info_this_stage.append(info_branch[idx])

    return info_this_stage


def beam_search_one_branch(cipher_char, cipher_text, current_plain_text, prev_hypothesis, model):
    info_branch = []
    for plain_char in letter:
        if plain_char in prev_hypothesis:
            continue
        hypothesis = {cipher_char: plain_char}
        new_seq, score = evaluate_new_hypothesis(hypothesis, cipher_text, current_plain_text, model)
        info = {'hypothesis': prev_hypothesis + plain_char,
                'sequence': new_seq,
                'score': score}
        info_branch.append(info)
    return info_branch


def evaluate_new_hypothesis(hypothesis, cipher_text, current_plain_text, model):
    # print(hypothesis)
    cipher_char = list(hypothesis.keys())[0]
    plain_char = hypothesis[cipher_char]
    indices = get_indices_of_char_in_string(cipher_char, cipher_text)
    current_plain_text = list(current_plain_text)
    assert len(indices) != 0, 'Debug????'
    for idx in indices:
        current_plain_text[idx] = plain_char

    input_tensor = torch.zeros(len(cipher_text)).long()
    for i, char in enumerate(current_plain_text):
        if char != '.':
            input_tensor[i] = letter.index(char)
        else:
            input_tensor[i] = -1

    new_seq, prob_list = generate_sequence(model, input_tensor)
    return new_seq, calc_score(prob_list)


def get_indices_of_char_in_string(char, s):
    indices = []
    for i in range(len(s)):
        if char == s[i]:
            indices.append(i)

    return indices
