import os

letter = 'abcdefghijklmnopqrstuvwxyz'
data_path = os.path.join(os.getcwd(), 'data')


class DeciphermentConfig:
    def __init__(self):
        self.nbeam = 1000
        self.ntext = None
