import os
import math
import numpy as np

from decipherment.decipherment_config import DeciphermentConfig
from decipherment.beam_search import beam_search
from decipherment.prepare_data import get_cipher_data, get_plain_data
from decipherment.metrics import calc_SER
from language_model.models.mlstm import mLSTM, StackedLSTM
from language_model.NLM_config import LMConfig, letter
from language_model.utils import load_model


def evaluate(cipher_text, plain_text, model, config):
    print(f'Cipher text: {cipher_text}')
    print(f'Plain text: {plain_text}')
    seq_list = beam_search(cipher_text, model, config)
    min_score = math.inf
    seq = None
    for sequence in seq_list:
        score = calc_SER(sequence, plain_text)
        if score < min_score:
            seq = sequence
            min_score = score
    print(f'Decoded sequence: {seq}')
    print(f'SER: {min_score}')
    return min_score


if __name__ == '__main__':
    nlm_config = LMConfig()
    decipher_config = DeciphermentConfig()

    cipher_data = get_cipher_data()
    plain_data = get_plain_data()
    ntext = decipher_config.ntext
    if ntext is not None:
        cipher_data = cipher_data[:ntext]
        plain_data = plain_data[:ntext]

    model_path = os.path.join(os.getcwd(), 'pretrained', 'mlstm_last.pt')
    model = StackedLSTM(mLSTM,
                        nlm_config.n_layers,
                        nlm_config.embed_size,
                        nlm_config.hidden_size,
                        len(letter),
                        nlm_config.dropout)
    model = load_model(model, model_path)

    score_list = []
    for i in range(len(cipher_data)):
        cipher_text = cipher_data[i]
        plain_text = plain_data[i]
        score_list.append(evaluate(cipher_text, plain_text, model, decipher_config))

    print(f'Average SER: {np.mean(score_list)}')
