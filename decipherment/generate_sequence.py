import torch
import torch.nn.functional as F

from language_model.models.mlstm import StackedLSTM
from language_model.utils import tensor_to_string

from typing import List, Tuple


def generate_sequence(model: StackedLSTM, input_tensor: torch.Tensor) -> Tuple[torch.Tensor, List[float]]:
    """
    Use Neural Language Model to generate a sequence from a partial hypothesis
    and return a new sequence and probabilities of each character
    :param model: Neural Language Model, here is mLSTM
    :param input_tensor: tensor with shape L (length of the cipher)
    :return: a new sequence from partial hypothesis, probabilities for each character in new sequence
    """
    prob_list = []
    last_out = input_tensor[0]
    output_tensor = torch.zeros(len(input_tensor)).long()
    output_tensor[0] = input_tensor[0]
    h_0, c_0 = model.state0(1)
    hidden = h_0, c_0
    for i in range(1, len(input_tensor)):
        is_hypothesis = input_tensor[i].item() != -1
        # inp = last_out.cuda()
        inp = last_out
        # print(inp.shape)

        hidden, out = model(inp, hidden)
        y_pred = F.softmax(out, dim=1)

        if is_hypothesis:
            last_out = input_tensor[i]
        else:
            last_out = torch.argmax(y_pred, dim=1)
        prob = y_pred[0, last_out.item()].item()
        prob_list.append(prob)
        output_tensor[i] = last_out

    output_sequence = tensor_to_string(output_tensor)

    return output_sequence, prob_list
