import numpy as np


def calc_score(prob_list):
    return -1 * np.sum(np.log2(prob_list))
